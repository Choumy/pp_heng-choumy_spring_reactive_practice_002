package com.example.webfluxdemo.service.cloudinstance

import com.example.webfluxdemo.handler.AppUser
import com.example.webfluxdemo.model.dto.CloudInstanceDto
import com.example.webfluxdemo.model.request.CloudInstanceRequest
import com.example.webfluxdemo.repository.CloudInstanceRepository
import com.example.webfluxdemo.repository.OperatingSystemRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class CloudInstanceServiceImpl(
    val cloudInstanceRepository: CloudInstanceRepository,
    val operatingSystemRepository: OperatingSystemRepository,
    @Qualifier("UserClient") val userClient: WebClient
) : CloudInstanceService {
    override fun create(cloudInstanceRequest: CloudInstanceRequest): Mono<CloudInstanceDto> {
        return cloudInstanceRepository
            .save(cloudInstanceRequest.toEntity())
            .map { res -> res.toDto() }
    }

    override fun findAll(): Flux<CloudInstanceDto> {
        val cloudInstanceFlux = cloudInstanceRepository
            .findAll()

        val osFlux = cloudInstanceFlux
            .flatMap {
                operatingSystemRepository.findById(it.operatingSystemId)
            }

        fun getAppUserById(id: String): Mono<AppUser> = userClient.get()
            .uri("/api/v1/users/{id}", id)
            .retrieve()
            .bodyToMono(AppUser::class.java)

        val cloudOS = cloudInstanceFlux.zipWith(osFlux)
            .map {
                val cloud = it.t1
                val myos = it.t2

                val cloudResponse = cloud.toDto()
                cloudResponse.operatingSystem = myos.toDto()

                cloudResponse
            }
        val appUserId: Flux<String> = cloudInstanceFlux.map { it.userId }
        return cloudOS.zipWith(appUserId).flatMap {
            val cloudos = it.t1
            val userId = it.t2
            val user = getAppUserById(userId)

            Mono.just(cloudos).zipWith(user).map {
                res ->
                val cloud = res.t1
                val userRes = res.t2
                    cloud.appUser = userRes
                cloud
            }
        }
    }


}